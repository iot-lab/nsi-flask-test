import datetime
import flask
from flask import jsonify, request, app


class Main:
    app = flask.Flask(__name__)  # Creates the Flask application object
    app.config["DEBUG"] = True

    # Readme
    dt = datetime.datetime.today()
    print("All days: http://127.0.0.1:5000/api/v1/resources/today/all")
    print("Today by ID: http://127.0.0.1:5000/api/v1/resources/today?id=2")
    print("Today by month and day: http://127.0.0.1:5000/api/v1/resources/today?month=" + '{:02d}'.format(
        dt.month) + "&day=" + '{:02d}'.format(dt.day))

    def __init__(self):

        # Test data for our catalog in the form of a list of dictionaries.
        # Jokes from here: https://www.rd.com/list/short-jokes/
        self.todays = [
            {'id': 0,
             'month': '05',
             'day': '04',
             'historic_event': '1670 – A royal charter granted the Hudsons Bay Company a monopoly in the fur trade in Ruperts Land (present-day Canada).',
             'joke': 'What’s the best thing about Switzerland? I don’t know, but the flag is a big plus.'},
            {'id': 1,
             'month': '05',
             'day': '05',
             'historic_event': '2010 – Mass protests in Greece erupt in response to austerity measures imposed by the government as a result of the Greek government-debt crisis.',
             'joke': 'I invented a new word! Plagiarism!'},
            {'id': 2,
             'month': '05',
             'day': '06',
             'historic_event': '2002– Founding of SpaceX.',
             'joke': 'Did you hear about the mathematician who’s afraid of negative numbers? He’ll stop at nothing to avoid them.'},
            {'id': 3,
             'month': '05',
             'day': '07',
             'historic_event': '2000 - Vladimir Putin is inaugurated as president of Russia.',
             'joke': 'How did the hacker escape from the police? He ransomware.'},
        ]

        @self.app.route('/', methods=['GET'])
        def __home():
            return self.home()

        @self.app.route('/api/v1/resources/today/all', methods=['GET'])
        def __api_all():
            return self.api_all()

        @self.app.route('/api/v1/resources/today', methods=['GET'])
        def __api_id():
            return self.api_id()

        self.app.run(host="0.0.0.0", port=5000, debug=True)
        #self.app.run(host="localhost", port=5000, debug=True)

    @staticmethod
    def home():
        return '''<h1>Today</h1>
        <p>A prototype API for finding out what happened on this day</p>'''

    def api_all(self):
        return jsonify(self.todays)

    def api_id(self):

        # Create an empty list for our results
        results = []

        # Check if an ID was provided as part of the URL.
        # If ID is provided, assign it to a variable.
        # If no ID is provided, display an error in the browser.
        if 'id' in request.args:
            id = int(request.args['id'])

            # Loop through the data and match results that fit the requested ID.
            # IDs are unique, but other fields might return many results
            for today in self.todays:
                if today['id'] == id:
                    results.append(today)
        else:
            # Month and day search
            if 'month' in request.args and 'day' in request.args:
                month = str(request.args['month'])
                day = str(request.args['day'])

                for today in self.todays:
                    if today['month'] == month and today['day'] == day:
                        results.append(today)

                result_length = len(results)
                if result_length == 0:
                    return "Error: Not yet implemented or not found"
            else:
                return "Error: No id, month or day field provided. Please specify."



        # Use the jsonify function from Flask to convert our list of
        # Python dictionaries to the JSON format.
        return jsonify(results)

Main()