import unittest
import json
from main import Main


class MainTest(unittest.TestCase):
    def setUp(self):
        self.ctx = Main.app.app_context()
        self.ctx.push()
        self.client = Main.app.test_client()

    def tearDown(self):
        self.ctx.pop()

    # test home #
    def test_home(self):
        """ Test Home will ho to home page of the flask API and check if the home str equals
        the content of the home page """

        home_str = '''<h1>Today</h1>
        <p>A prototype API for finding out what happened on this day</p>'''
        response = self.client.get("/", data={"content": home_str})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(home_str, response.get_data(as_text=True))

    # test all #
    def test_all(self):
        """ Test goes to all API and should then display the entire json object.
         The test checks if the first id equals 0. """

        response = self.client.get("/api/v1/resources/today/all")
        data = json.loads(response.get_data(as_text=True))
        self.assertEqual(data[0]['id'], 0)

    # test id #
    def test_id(self):
        """ Test should pull the json object for id 2. The test check that it gets
         the ID 2. """

        response = self.client.get("/api/v1/resources/today?id=2")
        data = json.loads(response.get_data(as_text=True))
        self.assertEqual(data[0]['id'], 2)

    # test month day #
    def test_month_day(self):
        """ Will send in a month and a day and want to get back if the month
         and day is present in the json response """

        response = self.client.get("/api/v1/resources/today?month=05&day=06")
        data = json.loads(response.get_data(as_text=True))
        self.assertEqual(data[0]['month'], "05")
        self.assertEqual(data[0]['day'], "06")


if __name__ == "__main__":
    unittest.main()